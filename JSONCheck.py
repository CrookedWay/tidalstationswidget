import glob
import json

jsonGlob = glob.glob("*.json")
for glob in jsonGlob:
    with open("ComparisonFile.txt", 'a') as file:
        file.write(str(glob))
        file.write("\n")
        json_data=open(glob)
        data = json.load(json_data)
        file.write(json.dumps(data, sort_keys=True, indent=4))
