import json
import pandas as pd
import glob


# df =pd.read_excel("RCP45.xlsx")
# # df = df[pd.notnull(df.index)]
# # df.index = df.index.map(int)
# df = df.to_json()
# print(df)
# with open("RCP45.json", 'w') as f:
#     f.write(df)

spreadsheets = glob.glob("*.xlsx")
for spreadsheet in spreadsheets:
    df =pd.read_excel(spreadsheet)
    df = df[pd.notnull(df.index)]
    df.index = df.index.map(int)
    df = df.to_json()
    with open(str(spreadsheet)[:-5]+".json", 'w') as f:
        f.write(df)

# df = pd.read_excel("27_station_annual_met_19yr_2000_2100.xlsx")
# with open("OutputJson", 'w') as file:
#     for z in list(df):
#         file.write(z)
#         file.write("\n")
#         cutDf = df[z]
#         values = list(cutDf.values)
#         for x, y in zip(values, range(2000, 2100)):
#             file.write(""""""""+str(y)+""""""""+": "+ str(x)+",")
#             file.write("\n")
#         file.write("\n")